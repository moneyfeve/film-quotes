// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { FirebaseConfig } from '../libs/environment/firebase-config.model';
import { KinopoiskConfig } from '../libs/environment/kinopoisk-config.model';

export const environment = {
  production: false,
  firebaseConfig: <FirebaseConfig>{
    apiKey: 'AIzaSyC5j2aCaMaikxriPnw208zUCFDuYNRvIsw',
    authDomain: 'film-by-words.firebaseapp.com',
    dataBaseUrl:
      'https://film-by-words-default-rtdb.europe-west1.firebasedatabase.app',
    projectId: 'film-by-words',
    storageBucket: 'film-by-words.appspot.com',
    messagingSenderId: '666567915586',
    appId: '1:666567915586:web:f7ef9320954ed80a3d55db',
  },
  kinopoiskConfig: <KinopoiskConfig>{
    apiKey: '938VZYY-W1P4Y8Z-PW5Y1YZ-7Q6QJHN',
    apiUrl: 'https://api.kinopoisk.dev/v1.4/',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
