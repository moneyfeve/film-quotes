import {FirebaseConfig} from "../libs/environment/firebase-config.model";

export const environment = {
  production: true,
  firebaseConfig: <FirebaseConfig> {
    apiKey: "AIzaSyC5j2aCaMaikxriPnw208zUCFDuYNRvIsw",
    authDomain: "film-by-words.firebaseapp.com",
    dataBaseUrl: "https://film-by-words-default-rtdb.europe-west1.firebasedatabase.app/",
    projectId: "film-by-words",
    storageBucket: "film-by-words.appspot.com",
    messagingSenderId: "666567915586",
    appId: "1:666567915586:web:f7ef9320954ed80a3d55db"
  }
};
