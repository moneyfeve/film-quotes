import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureSearchPageComponent } from './feature-search-page.component';

describe('FeatureSearchPageComponent', () => {
  let component: FeatureSearchPageComponent;
  let fixture: ComponentFixture<FeatureSearchPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureSearchPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeatureSearchPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
