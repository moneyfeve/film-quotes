import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {
  debounce,
  distinctUntilChanged,
  map,
  Observable,
  of,
  switchMap,
  tap,
  timer,
} from 'rxjs';
import { FormControl } from '@angular/forms';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';
import { Film, Films } from '../data-access-films/data-access-films.model';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PaginationData } from '../feature-pagination/pagination.model';
import { isObjectValuesEqual } from '../shared/util-compare/compare-object';

@Component({
  selector: 'app-feature-search-page',
  templateUrl: './feature-search-page.component.html',
  styleUrl: './feature-search-page.component.scss',
})
export class FeatureSearchPageComponent implements OnInit {
  @ViewChild('sectionWrapper') sectionWrapper: ElementRef | undefined;
  placeholderString: string = 'Название фильма или фразу';
  films$: Observable<Film[]> | null = null;
  isFoundSomething: boolean = false;
  isSearchFieldEmpty: boolean = true;
  isLoaded: boolean = false;
  isFirstLoadingData: boolean = true;
  searchControl: FormControl<string | null> = new FormControl('');
  paginationData: PaginationData = {
    page: null,
    pages: null,
  };

  constructor(
    private dataAccessFilmsService: DataAccessFilmsService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    const s = route.snapshot.queryParams['s'];
    if (!!s) {
      this.searchControl.setValue(s);
      this.isSearchFieldEmpty = false;
    }
  }
  ngOnInit(): void {
    this.films$ = this.route.queryParams.pipe(
      debounce(() => {
        if (!this.isFirstLoadingData) return timer(460);
        this.isFirstLoadingData = false;
        return timer(0);
      }),
      distinctUntilChanged(isObjectValuesEqual<Params>),
      tap(() => {
        this.isSearchFieldEmpty = !this.searchControl.value?.length;
        this.isLoaded = false;
      }),
      switchMap((data: Params): Observable<Film[] | []> => {
        if (!data['s']?.length) return of([]);
        return this.dataAccessFilmsService
          .getFilmsByName(data['s'], { p: data['p'] || 1 })
          .pipe(
            map((output: Films) => {
              this.paginationData = {
                page: output.page,
                pages: output.pages,
              };
              return output.films.map((film: Film) => {
                delete film.description;
                return film;
              });
            }),
          );
      }),
      tap((films: Film[]) => {
        this.isFoundSomething = !!films.length;
        this.isLoaded = true;
      }),
    );
  }

  addQueryParams(params: Params) {
    const queryParams = { ...this.route.snapshot.queryParams, ...params };
    if (!!params['s']) delete queryParams['p'];
    this.router.navigate([], { queryParams });
    this.scrollTop();
  }

  scrollTop() {
    this.sectionWrapper?.nativeElement.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth',
    });
  }

  navigateToFilm(filmId: string) {
    this.router.navigate(['films', filmId]);
  }
}
