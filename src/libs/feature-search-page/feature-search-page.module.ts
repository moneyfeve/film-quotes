import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureSearchPageComponent } from './feature-search-page.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiFilmCardModule } from '../ui-film-card/ui-film-card.module';
import { UiSiteLogoModule } from '../ui-site-logo/ui-site-logo.module';
import { FeaturePaginationModule } from '../feature-pagination/feature-pagination.module';

const routes: Routes = [
  {
    path: '',
    component: FeatureSearchPageComponent,
  },
];

@NgModule({
  declarations: [FeatureSearchPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    FeaturePaginationModule,
    UiFilmCardModule,
    UiSiteLogoModule,
  ],
  exports: [FeatureSearchPageComponent],
})
export class FeatureSearchPageModule {}
