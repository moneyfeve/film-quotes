import {
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'ui-search-list',
  templateUrl: './ui-search-list.component.html',
  styleUrl: './ui-search-list.component.scss',
})
export class UiSearchListComponent implements OnInit {
  @Input() placeholder: string | undefined;
  @Input() control: FormControl | undefined;
  @Input() defaultValue: string | null | undefined;
  @Input() hideList: boolean = false;
  @Input() dataList: any;
  @Output() itemSelected: EventEmitter<any> = new EventEmitter();

  @ContentChild(TemplateRef) templateRef: TemplateRef<any> | undefined;

  ngOnInit(): void {
    !!this.defaultValue && this.control?.setValue(this.defaultValue);
  }

  select(item: any) {
    this.itemSelected.emit(item);
  }
}
