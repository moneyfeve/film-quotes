import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiSearchListComponent } from './ui-search-list.component';

describe('UiSearchListComponent', () => {
  let component: UiSearchListComponent;
  let fixture: ComponentFixture<UiSearchListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiSearchListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiSearchListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
