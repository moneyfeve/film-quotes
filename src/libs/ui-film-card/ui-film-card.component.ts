import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Film } from '../data-access-films/data-access-films.model';

@Component({
  selector: 'app-ui-film-card',
  templateUrl: './ui-film-card.component.html',
  styleUrl: './ui-film-card.component.scss',
})
export class UiFilmCardComponent {
  @Input() film!: Film | null;
  @Output() linkAction: EventEmitter<any> = new EventEmitter();

  linkEmmit() {
    this.linkAction.emit(this.film?.id);
  }
}
