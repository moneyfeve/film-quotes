import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiFilmCardComponent } from './ui-film-card.component';

describe('UiFilmCardComponent', () => {
  let component: UiFilmCardComponent;
  let fixture: ComponentFixture<UiFilmCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiFilmCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiFilmCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
