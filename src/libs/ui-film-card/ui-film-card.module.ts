import {NgModule} from "@angular/core";
import {UiFilmCardComponent} from "./ui-film-card.component";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [UiFilmCardComponent],
    imports: [
        CommonModule
    ],
    exports: [UiFilmCardComponent]
})
export class UiFilmCardModule {}
