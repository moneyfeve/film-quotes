import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';
import { AuthFirebaseService } from '../../../projects/auth-firebase/src/lib/auth-firebase.service';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanActivate {
  constructor(
    private service: AuthFirebaseService,
    private router: Router,
  ) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (!this.service.isAuthenticated()) return true;
    this.router.navigate(['/admin'], {});
    return false;
  }
}
