import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthUser } from '../../../projects/auth-firebase/src/lib/auth-user.model';
import { AuthFirebaseService } from '../../../projects/auth-firebase/src/lib/auth-firebase.service';
import { take, tap } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';

@Component({
  selector: 'app-feature-login-page',
  templateUrl: './feature-login-page.component.html',
  styleUrl: './feature-login-page.component.scss',
})
export class FeatureLoginPageComponent {
  loginForm: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  navigateAfterLoginURL: string;

  constructor(
    private authService: AuthFirebaseService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private notificationCenter: NotificationCenterService,
  ) {
    this.navigateAfterLoginURL =
      this.activatedRoute.snapshot.queryParams['returnUrl'] || '/admin';
  }

  submitForm(data: any) {
    if (this.loginForm.invalid) return this.loginForm.markAllAsTouched();

    const user: AuthUser = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
    };

    this.authService
      .login(user)
      .pipe(take(1))
      .subscribe((data) => {
        this.loginForm.reset();
        this.notificationCenter.success('Вы успешно прошли аутентификацию');
        this.router.navigate([this.navigateAfterLoginURL], {});
      });
  }
}
