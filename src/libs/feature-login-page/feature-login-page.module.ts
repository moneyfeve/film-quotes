import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureLoginPageComponent } from './feature-login-page.component';
import { RouterModule, Routes } from '@angular/router';
import { UiSiteLogoModule } from '../ui-site-logo/ui-site-logo.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  {
    path: '',
    component: FeatureLoginPageComponent,
  },
];
@NgModule({
  declarations: [FeatureLoginPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UiSiteLogoModule,
    ReactiveFormsModule,
  ],
  exports: [FeatureLoginPageComponent],
})
export class FeatureLoginPageModule {}
