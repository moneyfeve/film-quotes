import { NgModule } from '@angular/core';
import { UiSiteLogoComponent } from './ui-site-logo.component';
import { RouterLink } from '@angular/router';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UiSiteLogoComponent],
  imports: [CommonModule, RouterLink],
  exports: [UiSiteLogoComponent],
})
export class UiSiteLogoModule {}
