import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiSiteLogoComponent } from './ui-site-logo.component';

describe('UiSiteLogoComponent', () => {
  let component: UiSiteLogoComponent;
  let fixture: ComponentFixture<UiSiteLogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiSiteLogoComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiSiteLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
