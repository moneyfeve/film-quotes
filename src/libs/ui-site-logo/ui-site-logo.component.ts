import { Component } from '@angular/core';

@Component({
  selector: 'ui-site-logo',
  templateUrl: './ui-site-logo.component.html',
  styleUrl: './ui-site-logo.component.scss',
})
export class UiSiteLogoComponent {}
