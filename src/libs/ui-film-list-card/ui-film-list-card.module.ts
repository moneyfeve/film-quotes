import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiFilmListCardComponent } from './ui-film-list-card.component';

@NgModule({
  declarations: [UiFilmListCardComponent],
  imports: [CommonModule],
  exports: [UiFilmListCardComponent],
})
export class UiFilmListCardModule {}
