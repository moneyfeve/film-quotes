import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiFilmListCardComponent } from './ui-film-list-card.component';

describe('UiFilmListCardComponent', () => {
  let component: UiFilmListCardComponent;
  let fixture: ComponentFixture<UiFilmListCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiFilmListCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiFilmListCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
