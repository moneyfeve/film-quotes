import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Film } from '../data-access-films/data-access-films.model';

@Component({
  selector: 'ui-film-list-card',
  templateUrl: './ui-film-list-card.component.html',
  styleUrl: './ui-film-list-card.component.scss',
})
export class UiFilmListCardComponent {
  @Input() film: Film | undefined;
  @Output() linkEmitter: EventEmitter<any> = new EventEmitter<any>();

  linkHandler() {
    this.linkEmitter.emit(this.film?.id);
  }
}
