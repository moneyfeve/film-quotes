export interface QuotesMap {
  id?: string;
  filmID: number;
  quotes: string[];
}

export interface FbCreateResponse {
  name: string;
}

export interface FbGetResponse {
  [key: string]: QuotesMap;
}
