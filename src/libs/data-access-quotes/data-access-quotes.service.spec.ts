import { TestBed } from '@angular/core/testing';

import { DataAccessQuotesService } from './data-access-quotes.service';

describe('DataAccessQuotesService', () => {
  let service: DataAccessQuotesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataAccessQuotesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
