import { Inject, Injectable } from '@angular/core';
import { FB_TOKEN } from '../environment/firebase-config.token';
import { FirebaseConfig } from '../environment/firebase-config.model';
import { HttpClient } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import {
  FbCreateResponse,
  FbGetResponse,
  QuotesMap,
} from './data-access-quotes.model';

@Injectable({
  providedIn: 'root',
})
export class DataAccessQuotesService {
  constructor(
    @Inject(FB_TOKEN) private firebaseConfig: FirebaseConfig,
    private http: HttpClient,
  ) {}

  getQuotes(): Observable<QuotesMap[]> {
    return this.http
      .get<FbGetResponse>(
        `${this.firebaseConfig.dataBaseUrl}/quotes.json?print=pretty`,
      )
      .pipe(
        map((res: FbGetResponse): QuotesMap[] => {
          const ids: string[] = Object.keys(res);
          return Object.values(res).map((quotesMap: QuotesMap, index) => {
            return { id: ids[index], ...quotesMap };
          });
        }),
      );
  }

  getFilmQuotes(filmId: number): Observable<QuotesMap | null> {
    return this.http
      .get<FbGetResponse>(
        `${this.firebaseConfig.dataBaseUrl}/quotes.json?orderBy="filmID"&equalTo=${filmId}&print=pretty`,
      )
      .pipe(
        map((res: FbGetResponse): QuotesMap | null => {
          const id = Object.keys(res)[0];
          if (!id) return null;
          return { id, ...res[id] };
        }),
      );
  }

  createQuotes(quotes: QuotesMap): Observable<QuotesMap> {
    return this.http
      .post<FbCreateResponse>(
        `${this.firebaseConfig.dataBaseUrl}/quotes.json`,
        quotes,
      )
      .pipe(
        map((res: FbCreateResponse) => {
          return {
            ...quotes,
            id: res.name,
          };
        }),
      );
  }

  updateQuotes(id: string, quotes: QuotesMap): Observable<QuotesMap> {
    return this.http
      .patch<QuotesMap>(
        `${this.firebaseConfig.dataBaseUrl}/quotes/${id}.json`,
        quotes,
      )
      .pipe(
        map((quotesMap: QuotesMap) => {
          return {
            ...quotesMap,
            id: id,
          };
        }),
      );
  }

  deleteQuotes(id: string) {
    return this.http.delete(
      `${this.firebaseConfig.dataBaseUrl}/quotes/${id}.json`,
    );
  }
}
