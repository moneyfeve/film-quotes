export const transformPagesToArray = (
  pages: number,
  page: number,
  maxLength: number,
) => {
  maxLength = Math.min(maxLength, pages);
  const half = Math.ceil(maxLength / 2);
  const startIndex = range(
    1,
    (page > half && page - (half - 1)) || 1,
    pages - (maxLength - 1),
  );
  return new Array(maxLength).fill(0).map((p, i) => startIndex + i);
};

export const range = (min: number, current: number, max: number) => {
  return Math.max(min, Math.min(current, max));
};
