import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeaturePaginationComponent } from './feature-pagination.component';

describe('FeaturePaginationComponent', () => {
  let component: FeaturePaginationComponent;
  let fixture: ComponentFixture<FeaturePaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeaturePaginationComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeaturePaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
