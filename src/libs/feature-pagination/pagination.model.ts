export interface PaginationData {
  pages: number | null;
  page: number | null;
}
