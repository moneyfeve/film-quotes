import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { PaginationData } from './pagination.model';
import { transformPagesToArray } from './pagination.utils';

@Component({
  selector: 'pagination',
  templateUrl: './feature-pagination.component.html',
  styleUrl: './feature-pagination.component.scss',
})
export class FeaturePaginationComponent implements OnChanges {
  @Input() data: PaginationData | undefined;
  @Output() dataChange: EventEmitter<PaginationData> =
    new EventEmitter<PaginationData>();

  pagesArray: number[] = [];
  maxLength: number = 7;

  ngOnChanges(changes: SimpleChanges) {
    const page = changes['data'].currentValue?.page;
    const pages = changes['data'].currentValue?.pages;
    if (!!pages && !!page) {
      this.pagesArray = transformPagesToArray(pages, page, this.maxLength);
    }
  }

  changePage(page: number) {
    if (!this.data) return;
    this.dataChange.emit({ page, pages: this.data.pages });
  }
}
