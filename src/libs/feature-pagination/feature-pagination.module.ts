import { NgModule } from '@angular/core';
import { FeaturePaginationComponent } from './feature-pagination.component';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [FeaturePaginationComponent],
  imports: [CommonModule],
  exports: [FeaturePaginationComponent],
})
export class FeaturePaginationModule {}
