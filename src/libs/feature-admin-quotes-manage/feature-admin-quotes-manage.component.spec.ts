import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureAdminQuotesManageComponent } from './feature-admin-quotes-manage.component';

describe('FeatureAdminQuotesManageComponent', () => {
  let component: FeatureAdminQuotesManageComponent;
  let fixture: ComponentFixture<FeatureAdminQuotesManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureAdminQuotesManageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeatureAdminQuotesManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
