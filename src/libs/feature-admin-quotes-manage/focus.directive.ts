import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[focus]',
  standalone: true,
})
export class FocusDirective {
  constructor(private elementRef: ElementRef) {}

  @Input() set focus(condition: boolean) {
    condition && this.elementRef.nativeElement.focus();
  }
}
