import { Injectable } from '@angular/core';

@Injectable()
export class QuotesService {
  private _quotes: string[] = [];

  init(quotes: string[]): void {
    this._quotes = quotes;
  }

  getQuotes(): string[] {
    return this._quotes;
  }

  getQuote(i: number): string {
    return this._quotes[i] || '';
  }

  updateQuote(i: number, text: string) {
    this._quotes[i] && (this._quotes[i] = text);
  }

  addQuote(text: string) {
    this._quotes.push(text);
  }

  removeQuote(i: number) {
    this._quotes.splice(i, 1);
  }

  resetQuotes() {
    this._quotes = [];
  }
}
