import { NgModule } from '@angular/core';
import { FeatureAdminQuotesManageComponent } from './feature-admin-quotes-manage.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { QuotesService } from './quotes.service';
import { FocusDirective } from './focus.directive';

@NgModule({
  declarations: [FeatureAdminQuotesManageComponent],
  imports: [CommonModule, ReactiveFormsModule, FocusDirective],
  providers: [QuotesService],
  exports: [FeatureAdminQuotesManageComponent],
})
export class FeatureAdminQuotesManageModule {}
