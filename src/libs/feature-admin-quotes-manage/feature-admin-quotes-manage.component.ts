import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';
import { QuotesService } from './quotes.service';

@Component({
  selector: 'feature-admin-quotes-manage',
  templateUrl: './feature-admin-quotes-manage.component.html',
  styleUrl: './feature-admin-quotes-manage.component.scss',
})
export class FeatureAdminQuotesManageComponent implements OnChanges {
  @Input() quotes: string[] | undefined;
  quoteNameControl: FormControl<string | null> = new FormControl('');
  isQuoteEditingID: number | null = null;

  constructor(
    private notificationCenter: NotificationCenterService,
    public readonly quotesService: QuotesService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('quotes' in changes) {
      this.quotesService.init(changes?.['quotes'].currentValue);
    }
  }

  addQuote(text: string | null) {
    if (!text) {
      return this.notificationCenter.info('Введите цитату');
    }
    this.quotesService.addQuote(text);
    this.quoteNameControl.reset();
  }

  startEditingQuoteMode(i: number, event: Event) {
    if (i === this.isQuoteEditingID) return;
    this.isQuoteEditingID = i;
  }
  stopEditingQuoteMode(i: number, event: Event) {
    this.isQuoteEditingID = null;
    this.quotesService.updateQuote(
      i,
      (event.target as HTMLInputElement).innerText,
    );
  }
}
