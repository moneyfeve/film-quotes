import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  filmDataResolver,
  quotesMapResolver,
} from '../feature-admin-film-manage/film-data.resolver';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/admin/films',
  },
  {
    path: 'films',
    children: [
      {
        path: '',
        loadChildren: () =>
          import(
            '../feature-admin-films-page/feature-admin-films-page.module'
          ).then((m) => m.FeatureAdminFilmsPageModule),
      },
      {
        path: 'add',
        loadChildren: () =>
          import(
            '../feature-admin-film-manage/feature-admin-film-manage.module'
          ).then((m) => m.FeatureAdminFilmManageModule),
      },
      {
        path: 'edit',
        children: [
          {
            path: ':filmID',
            loadChildren: () =>
              import(
                '../feature-admin-film-manage/feature-admin-film-manage.module'
              ).then((m) => m.FeatureAdminFilmManageModule),
            resolve: {
              filmData: filmDataResolver,
              quotesMap: quotesMapResolver,
            },
          },
          { path: '**', redirectTo: '/admin/films' },
        ],
      },
    ],
  },
];

interface NotificationService {
  info(text: string): void;
  success(text: string): void;
  warning(text: string): void;
}
export const NOTIFY_TOKEN: InjectionToken<NotificationService> =
  new InjectionToken<NotificationService>('NotificationService');

@NgModule({
  declarations: [],
  imports: [CommonModule, RouterModule.forChild(routes)],
  providers: [
    {
      provide: NOTIFY_TOKEN,
      useExisting: NotificationCenterService,
    },
  ],
})
export class FeatureAdminShellModule {}
