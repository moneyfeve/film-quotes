import {NgModule} from "@angular/core";
import {UiFilmPageCardComponent} from "./ui-film-page-card.component";
import {CommonModule} from "@angular/common";

@NgModule({
    declarations: [UiFilmPageCardComponent],
    imports: [CommonModule],
    exports: [UiFilmPageCardComponent]
})
export class UiFilmPageCardModule {

}
