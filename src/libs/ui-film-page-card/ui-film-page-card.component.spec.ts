import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiFilmPageCardComponent } from './ui-film-page-card.component';

describe('UiFilmPageCardComponent', () => {
  let component: UiFilmPageCardComponent;
  let fixture: ComponentFixture<UiFilmPageCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiFilmPageCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiFilmPageCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
