import { Component, Input } from '@angular/core';
import { Film } from '../data-access-films/data-access-films.model';

@Component({
  selector: 'ui-film-page-card',
  templateUrl: './ui-film-page-card.component.html',
  styleUrl: './ui-film-page-card.component.scss',
})
export class UiFilmPageCardComponent {
  @Input() film!: Film | null;
}
