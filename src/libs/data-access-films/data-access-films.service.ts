import { Injectable } from '@angular/core';
import { BackendService } from './backend.service';
import { Observable } from 'rxjs';
import { Film, Films, Filters, BaseFilters } from './data-access-films.model';

@Injectable({
  providedIn: 'root',
})
export class DataAccessFilmsService {
  constructor(private backendService: BackendService) {}

  getFilmsByName(query: string, options?: BaseFilters): Observable<Films> {
    const data: Filters = {
      ...options,
      query,
    };
    return this.backendService.getFilms(data);
  }

  getFilmsByIds(ids: number[], options?: BaseFilters): Observable<Films> {
    const data: Filters = {
      ...options,
      ids,
    };
    return this.backendService.getFilms(data);
  }

  getFilms(data: Filters): Observable<Films> {
    return this.backendService.getFilms(data);
  }

  getFilm(id: number): Observable<Film | null> {
    return this.backendService.getFilm(id);
  }
}
