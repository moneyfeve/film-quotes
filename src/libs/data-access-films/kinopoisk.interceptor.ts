import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Inject, Injectable } from '@angular/core';
import { KINOPOISK_TOKEN } from '../environment/kinopoisk-config.token';
import { KinopoiskConfig } from '../environment/kinopoisk-config.model';

@Injectable()
export class KinopoiskInterceptor implements HttpInterceptor {
  constructor(
    @Inject(KINOPOISK_TOKEN) private kinopoiskConfig: KinopoiskConfig,
  ) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (!req.url.includes(this.kinopoiskConfig.apiUrl)) return next.handle(req);
    const authReq = req.clone({
      headers: req.headers.set('X-API-KEY', this.kinopoiskConfig.apiKey),
    });
    return next.handle(authReq);
  }
}
