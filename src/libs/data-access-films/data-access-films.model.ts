import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';

export interface Film {
  id: number;
  posterSrc: string | null;
  name: string;
  year: number;
  genre: string[];
  description?: string;
  quotesMap?: QuotesMap | null;
}

export interface Films {
  films: Film[];
  page: number;
  pages: number;
}

export interface BaseFilters {
  p?: number;
  limit?: number;
}

export interface FiltersByName extends BaseFilters {
  query: string;
}

export interface FiltersByIds extends BaseFilters {
  ids: number[];
}

export type Filters = FiltersByName | FiltersByIds;
