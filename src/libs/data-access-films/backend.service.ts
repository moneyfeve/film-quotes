import { Inject, Injectable } from '@angular/core';
import { FB_TOKEN } from '../environment/firebase-config.token';
import { FirebaseConfig } from '../environment/firebase-config.model';
import { HttpClient } from '@angular/common/http';
import { delay, map, Observable, of } from 'rxjs';
import {
  Film,
  Films,
  Filters,
  FiltersByIds,
  FiltersByName,
} from './data-access-films.model';
import { KINOPOISK_TOKEN } from '../environment/kinopoisk-config.token';
import { KinopoiskConfig } from '../environment/kinopoisk-config.model';
import { Params } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class BackendService {
  constructor(
    @Inject(FB_TOKEN) private firebaseConfig: FirebaseConfig,
    @Inject(KINOPOISK_TOKEN) private kinopoiskConfig: KinopoiskConfig,
    private http: HttpClient,
  ) {}

  private transformOutputFilmDataToFilmData(film: any): Film {
    return {
      id: film.id,
      posterSrc:
        film.poster?.previewUrl ||
        'https://www.kinopoisk.ru/U14r9I248/23c60bJhIP/Q6UC7G5b8gJiOVbosBHhsSM5MRGqdYrtPIWBjRunDIZOU9ZkXl0egFGVz0GRWTmDKCGpAOFQ_dV3fjk5zyQDGEe5Su4tTfQeOShO4LBa57s7cidQq22V5Ayqr6Uqk26ydXvxXkxEDYFZk8xDjlh9gzdurxzdILTxRSM5O9EjCj1JvFquNGPeF2EgTeKpWfySIFwSRMxasvMAraq0Oa5Zr2la_kV9dij9SLnXY6peWrYfMJIv1AbVyHUeDiTFNzISX45BtixT2B5AMVHZmlrJrjJAI2XrfYjQGqebnB60dpFMWuhYd3YI40Kj9mqQdHuJJjW5E84Y1cl0Gh4h1DwWP3e6XYcGYdkyWA1U87Vt7aQjXCVK3HeW9RSQjZMtjnmHZGnPXEtlPMlzmdV_v29atSsaqD7RKuT-UTYvJNkmAShBo0C2BUPRFl8uccmFdfunJHs2eOpTlsYdnrevCqZgsmtXz1p-XTTbepTuTptSZKcIK5EAyC_67ng2KDHyHDEif55XrCtvwR9MO0PPunPdswNABnbOR4fSFKGMtBurcIZNSsBGYnQJxEeU-ESwdFqfKCGnJM0a8cx6CDM9xjYKOnu3SZQ1ZOMjZTZ06rxuwrEffxVg51SQ0gaZoKk6v3iIX0TQenFfPctDiuZWjltOhwoSuRHmK83bWig-ANMiHApQm26vL27cJkIATeW8ftqhE3c6TNNQrPMShIWAH5ZRgllu83thcBTkXpTHQJtZTqQUEbMkyhb_yF8kIgjWKAkMV5FgqTNX2hBsKVP4q235vRVXOEHQarTJA5GeiiCle4NfSe10ekcDzHaT0WKrUm6nOiOnJdAxw9RGBhse8DgzLkWjcqEoVPUeYAt6zItw1pEicgVn7lWxwjGktJQ8iW6Jcmv7ZG5dH-pKtNpeiXZwqQoqqyboBdfbURMaHvcBKitWkGmKLVPXP0UfT-WkZte1B0ADevFQlPQAkbuHAZ1xrmtzymVBYxb5U6rrRI5kc7MXMZkB5zDEwlsrKB7JLQ46fZlaoQ931SNGMlrYv0f8pCNmF0joZKT1Ha-JgSKLW7ZJdPZ7fnwM_3qe61i_T2WzEietJuQ-_tF7FjE9ygsTAlq8e68Ua-sEZj9o84ZswLUBQDFnwmmH8AasjJYuuVOhdmn9ZEt2A-5_icZJlmRashQXhwDWM-DcaAIROME-Ni96nFuELmz0KUALVPuyXuGxJVoFePJxiuw-uqyCDKFMkXpp6FRrQQrER5jMSJVHWaoyJ4gC2hj-1VcaAQ3nFhMKYLB_iQ504DVlMVvmvHfejgJECkXne6ftOYOXkBKVS61te_NGX3Yd6UGd6Hi9fUGZFhCSI-QP5P12KS0e7jgXKmu7Sq4oY-k2ZihK7ptLwYYAby5a6EaTwT2-pooiv2CRelj7U0pDEdd_qthfuGd8qR0LlyDoMNn2SgQQDO40CRJrr1uFOEP2Blk6UvSKc-qhFlswTNFbh8cljKG0Hb9LoXZ9zGVzaTbKfLHJeL1cfLkFKLIa9zXO2nMiDh7mKAocdLx0tDtTwxNeM2z7g3bqkSN0DWvxZ43FIIOekg6IbIhtadNDanA9_kKMylakZ0CRGTqeE_oF085VMww7zRo0EFCQSLIMeNIdeTtD5JR1-pYHfC9RzV-EwBWxi6s8uVWWcHrdfGp_C_x3rdpmqWd4sxMJqAzwFd3yWQQhB9YhChR7lH2LI2PRIVgIUPmRbMuEOUUISNRXsvIJl52vIrpEkk9ZzHdCVTHCd7XHQ7NhW6EvFrgm2hH_8EYQJDnUDwk4f6F0uxJP4iFXPmbWpGXfsS9uFHTVVo3TEreXiQWqcYtobvxTWWUKxUOc4EqddnqTEjCKB8wH_N1YGiIxyQcwE0alX40JS_sfdQtF9JlP3pcueyxt4G2K8TWUqZcKukyUXnXTU2pfL-V-oupqk1ZxkgwZuz_hMtrSXT0hHs4tIA9kolC0C1vxIl4pQOuEVs69Elo6SPJOrvUxg5aFO5lQg1ts9nxORBHfdKvQZpF6foQvKawx6Qbu8Hs1JwjIHDQeWZRTkgh2_zVDGmjIhlPjgxhBB3juWqT6GpeDlhyeWKx_Xf5SY3Eb2Wqi-2i3WnOfOiSRMOoe_PlSGAQM0RszCl2eb5Yba9EAWRpj7IBXwJYPdy9V10qR0gKyi68Gu0yETU3deVRcLfxxu-pVtkZ-hw8JvibyEtD0RTonLcsEJBJZom6BKkfGP3MPcsOSR_qXJF0OQ9dqltEHlL6vJKRzq1Zz2VZ9VS38U5_RarF9YJweCokc7R7b20Q9JiP5Ky4IS6RUgQRI1Al9AGbTu276uQFfCGnGRJbrK42LhRGYdI53Re9nd3ws9nmK4USuR1KSGyysG-Yb59x2BjgY6ScrOUaSa4YXb_4XRTpR0JdV6qICUwlT8Uqp7heQubUkhmyBRHH-W01hN8VImeZ5m2N9lTk2syH1CtbIRhIGKsElPhFCrmCiE3XSGkIRcNWkTMq5LV4Sc8FRpfkKrJyQHrZsrm5G-0J6WA3mRojIXZ9FYL4ZFLgd0Tbc7mUbGjzjHi0jfqdUqglr1ztPAm3Dv3H6nzFMJ2rwV5XBN7OGsRCUW6pzeut4eV0S9liU0nSYXHuYPgeWB-Ao5tplJxkS2A8EPninTZIlZd0nfhBhxZVzyYMuXS1q0kyB6xiThqgLgFaQXWjCQH1SG811iO1_llRkuhowiQX2JvDUYikLMuk3IwFKk3u7M2bxJUIxduOZaOaZMUMsQ_R8kekZr5ecD655onRxwWlqUDT6cL_IfJJ_WL4ZELwG5Rv74FkdIR_YGDMIeoFWggNp3SdPO0DOgk_jtx55JVjHUKrkMpm9gAerdLBETtFZdHY5xWuC50SVTUe8GRSvJ9EF0O1yPyIu6AYiLmmuWooNZ-o5WBZp5aZ36IE0fAB6x2yOwACCoaIilVekaXniQG15LexDl_NJv0FbsRskqw7uPPLMRhY',
      name: film.name || film.alternativeName,
      year: film.year,
      genre: film.genres.map((item: { name: string }) => item.name),
      description: film.description,
    };
  }

  private transformObjectToUrlParams<T>(obj: T): string {
    let arr: string[] = [];
    for (let p in obj) {
      arr.push(`${p}=${obj[p]}`);
    }
    return arr.join('&');
  }

  getFilms(filters: Filters): Observable<Films> {
    const urlParams: Params = {};
    urlParams['page'] = filters.p || 1;
    urlParams['limit'] = filters.limit || 8;

    if ('ids' in filters && typeof filters.ids !== 'string') {
      urlParams['id'] = filters.ids.map((id) => `${id}`).join('&id=');
    }

    if ('query' in filters) {
      urlParams['query'] = filters.query || '';
    }

    const path: string = 'ids' in filters ? 'movie' : 'movie/search';
    const urlParamsString: string = this.transformObjectToUrlParams(urlParams);

    return this.http
      .get(
        encodeURI(`${this.kinopoiskConfig.apiUrl}${path}?${urlParamsString}`),
      )
      .pipe(
        map((data: any) => {
          return {
            films: data.docs.map(this.transformOutputFilmDataToFilmData),
            page: data.page,
            pages: data.pages,
          };
        }),
      );
  }
  getFilm(id: number): Observable<Film | null> {
    return this.http
      .get(encodeURI(`${this.kinopoiskConfig.apiUrl}movie/${id}`))
      .pipe(
        map((data: any): Film | null => {
          if (data.statusCode === 400) return null;
          return this.transformOutputFilmDataToFilmData(data);
        }),
      );
  }
}
