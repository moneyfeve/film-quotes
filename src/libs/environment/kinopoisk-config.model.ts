export interface KinopoiskConfig {
  apiKey: string;
  apiUrl: string;
}
