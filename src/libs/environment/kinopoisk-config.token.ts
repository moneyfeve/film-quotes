import { InjectionToken } from '@angular/core';
import { KinopoiskConfig } from './kinopoisk-config.model';

export const KINOPOISK_TOKEN = new InjectionToken<KinopoiskConfig>(
  'app.kinopoiskConfig.token',
);
