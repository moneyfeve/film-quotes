import {InjectionToken} from "@angular/core";
import {FirebaseConfig} from "./firebase-config.model";

export const FB_TOKEN = new InjectionToken<FirebaseConfig>('app.firebaseConfig.token')
