export interface FirebaseConfig {
  apiKey: string;
  authDomain: string;
  projectId: string;
  dataBaseUrl: string;
  storageBucket: string;
  messagingSenderId: string;
  appId: string;
}
