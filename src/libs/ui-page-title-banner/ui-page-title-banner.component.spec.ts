import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UiPageTitleBannerComponent } from './ui-page-title-banner.component';

describe('UiPageTitleBannerComponent', () => {
  let component: UiPageTitleBannerComponent;
  let fixture: ComponentFixture<UiPageTitleBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiPageTitleBannerComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(UiPageTitleBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
