import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-page-title-banner',
  templateUrl: './ui-page-title-banner.component.html',
  styleUrl: './ui-page-title-banner.component.scss',
})
export class UiPageTitleBannerComponent {
  @Input() title: string = '';
}
