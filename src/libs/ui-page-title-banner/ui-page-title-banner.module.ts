import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiPageTitleBannerComponent } from './ui-page-title-banner.component';

@NgModule({
  declarations: [UiPageTitleBannerComponent],
  imports: [CommonModule],
  exports: [UiPageTitleBannerComponent],
})
export class UiPageTitleBannerModule {}
