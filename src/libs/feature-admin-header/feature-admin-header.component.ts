import { Component } from '@angular/core';
import { AuthFirebaseService } from '../../../projects/auth-firebase/src/lib/auth-firebase.service';
import { Router } from '@angular/router';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';

@Component({
  selector: 'admin-header',
  templateUrl: './feature-admin-header.component.html',
  styleUrl: './feature-admin-header.component.scss',
})
export class FeatureAdminHeaderComponent {
  constructor(
    private readonly service: AuthFirebaseService,
    private router: Router,
    private notificationCenter: NotificationCenterService,
  ) {}

  logout(e: Event) {
    e.preventDefault();
    this.service.logout();
    this.notificationCenter.success('Вы успешно вышли из аккаунта');
    this.router.navigate(['/']);
  }
}
