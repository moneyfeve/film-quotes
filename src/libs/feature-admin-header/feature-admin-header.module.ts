import { NgModule } from '@angular/core';
import { FeatureAdminHeaderComponent } from './feature-admin-header.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UiSiteLogoModule } from '../ui-site-logo/ui-site-logo.module';

@NgModule({
  declarations: [FeatureAdminHeaderComponent],
  imports: [CommonModule, RouterModule, UiSiteLogoModule],
  exports: [FeatureAdminHeaderComponent],
})
export class FeatureAdminHeaderModule {}
