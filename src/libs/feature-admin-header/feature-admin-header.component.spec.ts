import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureAdminHeaderComponent } from './feature-admin-header.component';

describe('FeatureAdminHeaderComponent', () => {
  let component: FeatureAdminHeaderComponent;
  let fixture: ComponentFixture<FeatureAdminHeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureAdminHeaderComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeatureAdminHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
