import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureFilmPageComponent } from './feature-film-page.component';

describe('FeatureFilmPageComponent', () => {
  let component: FeatureFilmPageComponent;
  let fixture: ComponentFixture<FeatureFilmPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureFilmPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeatureFilmPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
