import { ResolveFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';
import { Film } from '../data-access-films/data-access-films.model';
import { catchError, combineLatestWith, EMPTY, map, tap } from 'rxjs';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';

export const filmDataResolver: ResolveFn<Film | null> = (route, state) => {
  const router: Router = inject(Router);
  const notificationCenter: NotificationCenterService = inject(
    NotificationCenterService,
  );
  const id: number = +route.paramMap.get('id')!;
  const redirectToHome = () => {
    router.navigate(['/']);
  };

  return inject(DataAccessFilmsService)
    .getFilm(id)
    .pipe(
      combineLatestWith(inject(DataAccessQuotesService).getFilmQuotes(id)),
      tap(([film, quotesMap]: [Film | null, QuotesMap | null]) => {
        if (!film) redirectToHome();
      }),
      map(([film, quotesMap]: [Film | null, QuotesMap | null]) => {
        !!film && (film.quotesMap = quotesMap);
        return film;
      }),
      catchError(() => {
        redirectToHome();
        notificationCenter.warning('Такого фильма в нашей библиотеке нету!');
        return EMPTY;
      }),
    );
};
