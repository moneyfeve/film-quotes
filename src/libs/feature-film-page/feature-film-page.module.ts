import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureFilmPageComponent } from './feature-film-page.component';
import { RouterModule, Routes } from '@angular/router';
import { UiSiteLogoModule } from '../ui-site-logo/ui-site-logo.module';
import { UiFilmPageCardModule } from '../ui-film-page-card/ui-film-page.card.module';
import { filmDataResolver } from './film-data.resolver';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/',
  },
  {
    path: ':id',
    component: FeatureFilmPageComponent,
    resolve: {
      filmData: filmDataResolver,
    },
  },
];

@NgModule({
  declarations: [FeatureFilmPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UiSiteLogoModule,
    UiFilmPageCardModule,
  ],
  exports: [FeatureFilmPageComponent],
})
export class FeatureFilmPageModule {}
