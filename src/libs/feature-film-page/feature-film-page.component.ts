import { Component } from '@angular/core';
import { ActivatedRoute, Data } from '@angular/router';
import { map, Observable } from 'rxjs';
import { Film } from '../data-access-films/data-access-films.model';

@Component({
  selector: 'feature-film-page',
  templateUrl: './feature-film-page.component.html',
  styleUrl: './feature-film-page.component.scss',
})
export class FeatureFilmPageComponent {
  $film: Observable<Film | null> | undefined;
  constructor(private route: ActivatedRoute) {
    this.$film = this.route.data.pipe(
      map((data: Data) => {
        return data?.['filmData'];
      }),
    );
  }
}
