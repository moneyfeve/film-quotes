import { Component, OnInit } from '@angular/core';
import { map, mergeWith, Observable, Subject, switchMap, tap } from 'rxjs';
import { Film } from '../data-access-films/data-access-films.model';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';
import { FeatureAdminFilmsPageService } from './feature-admin-films-page.service';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';

@Component({
  selector: 'app-feature-admin-films-page',
  templateUrl: './feature-admin-films-page.component.html',
  styleUrl: './feature-admin-films-page.component.scss',
})
export class FeatureAdminFilmsPageComponent implements OnInit {
  films$: Observable<Film[]> | undefined;
  filmsAfterDeleted$: Observable<Film[]> | undefined;
  deleteQuotesIDSubject: Subject<string> = new Subject();
  constructor(
    private featureService: FeatureAdminFilmsPageService,
    private dataAccessQuotessService: DataAccessQuotesService,
    private notiicationCenter: NotificationCenterService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.filmsAfterDeleted$ = this.deleteQuotesIDSubject.asObservable().pipe(
      switchMap((id: string) => this.dataAccessQuotessService.deleteQuotes(id)),
      tap(() => {
        this.notiicationCenter.success('Цитаты к фильму были успешно удалены');
      }),
      switchMap(() => this.featureService.getFilmsWithQuotes()),
    );

    this.films$ = this.route.data.pipe(
      map((data: Data) => {
        return data['filmsData'];
      }),
      mergeWith(this.filmsAfterDeleted$),
    );
  }

  remove(id: string) {
    if (!id.length) return;
    this.deleteQuotesIDSubject.next(id);
  }
  linkToFilm(id: string) {
    this.router.navigate(['/', 'films', id]);
  }
}
