import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureAdminFilmsPageComponent } from './feature-admin-films-page.component';

describe('FeatureAdminFilmsPageComponent', () => {
  let component: FeatureAdminFilmsPageComponent;
  let fixture: ComponentFixture<FeatureAdminFilmsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureAdminFilmsPageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FeatureAdminFilmsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
