import { ResolveFn } from '@angular/router';
import { combineLatestWith, map, of, switchMap } from 'rxjs';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { Film, Films } from '../data-access-films/data-access-films.model';
import { inject } from '@angular/core';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';
import { FeatureAdminFilmsPageService } from './feature-admin-films-page.service';

export const filmsDataResolver: ResolveFn<Film[]> = (route, state) => {
  const featureService: FeatureAdminFilmsPageService = inject(
    FeatureAdminFilmsPageService,
  );
  return featureService.getFilmsWithQuotes();
};
