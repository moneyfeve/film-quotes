import { NgModule } from '@angular/core';
import { FeatureAdminFilmsPageComponent } from './feature-admin-films-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { UiPageTitleBannerModule } from '../ui-page-title-banner/ui-page-title-banner.module';
import { UiFilmListCardModule } from '../ui-film-list-card/ui-film-list-card.module';
import { filmsDataResolver } from './films-data.resolver';
import { FeatureAdminFilmsPageService } from './feature-admin-films-page.service';

const routes: Routes = [
  {
    path: '',
    component: FeatureAdminFilmsPageComponent,
    resolve: {
      filmsData: filmsDataResolver,
    },
  },
];
@NgModule({
  declarations: [FeatureAdminFilmsPageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    UiPageTitleBannerModule,
    UiFilmListCardModule,
  ],
  providers: [FeatureAdminFilmsPageService],
  exports: [FeatureAdminFilmsPageComponent],
})
export class FeatureAdminFilmsPageModule {}
