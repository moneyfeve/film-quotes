import { Injectable } from '@angular/core';
import { combineLatestWith, map, of, switchMap } from 'rxjs';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { Film, Films } from '../data-access-films/data-access-films.model';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';

@Injectable()
export class FeatureAdminFilmsPageService {
  constructor(
    private dataAccessQuotesService: DataAccessQuotesService,
    private dataAccessFilmsService: DataAccessFilmsService,
  ) {}

  getFilmsWithQuotes() {
    return this.dataAccessQuotesService.getQuotes().pipe(
      switchMap((quotesMap: QuotesMap[]) => {
        const ids: number[] = Object.values(quotesMap).map((q) => q.filmID);
        return of(quotesMap).pipe(
          combineLatestWith(this.dataAccessFilmsService.getFilmsByIds(ids)),
        );
      }),
      map(([quotesMaps, films]: [QuotesMap[], Films]) => {
        const filmIds = quotesMaps.map((quotesMap) => quotesMap.filmID);
        return films.films.map((film: Film) => {
          const filmIdIndex = filmIds.indexOf(film.id);
          if (filmIdIndex === -1) return film;
          film.quotesMap = quotesMaps[filmIdIndex];
          return film;
        });
      }),
    );
  }
}
