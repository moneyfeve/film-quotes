export const isObjectValuesEqual = <T extends { [key: string]: any }>(
  o1: T,
  o2: T,
): boolean => {
  let isEqual: boolean = true;

  const o1Vals = Object.values(o1);
  const o2Vals = Object.values(o2);

  if (o1Vals.length != o2Vals.length) return false;

  for (let p in o1) {
    if (o1[p] !== o2[p]) {
      isEqual = false;
      break;
    }
  }

  return isEqual;
};
