import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FeatureAdminFilmManageComponent } from './feature-admin-film-manage.component';
import { RouterModule, Routes } from '@angular/router';
import { UiPageTitleBannerModule } from '../ui-page-title-banner/ui-page-title-banner.module';
import { UiSearchListComponent } from '../ui-form-controls/ui-search-list/ui-search-list.component';
import { FeatureAdminQuotesManageModule } from '../feature-admin-quotes-manage/feature-admin-quotes-manage.module';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: FeatureAdminFilmManageComponent,
  },
];

@NgModule({
  declarations: [FeatureAdminFilmManageComponent, UiSearchListComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    UiPageTitleBannerModule,
    FeatureAdminQuotesManageModule,
  ],
  exports: [FeatureAdminFilmManageComponent],
})
export class FeatureAdminFilmManageModule {}
