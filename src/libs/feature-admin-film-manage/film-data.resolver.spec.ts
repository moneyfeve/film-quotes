import { TestBed } from '@angular/core/testing';
import { ResolveFn } from '@angular/router';

import { filmDataResolver } from './film-data.resolver';

describe('filmDataResolver', () => {
  const executeResolver: ResolveFn<boolean> = (...resolverParameters) => 
      TestBed.runInInjectionContext(() => filmDataResolver(...resolverParameters));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    expect(executeResolver).toBeTruthy();
  });
});
