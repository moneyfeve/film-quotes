import { ResolveFn, Router } from '@angular/router';
import { inject } from '@angular/core';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { Film } from '../data-access-films/data-access-films.model';
import { catchError, EMPTY, tap } from 'rxjs';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';
import { NOTIFY_TOKEN } from '../feature-admin-shell/feature-admin-shell.module';

export const filmDataResolver: ResolveFn<Film | null> = (route, state) => {
  const dataAccessFilmsService = inject(DataAccessFilmsService);
  const router = inject(Router);
  const notificationCenter = inject(NOTIFY_TOKEN);
  return dataAccessFilmsService.getFilm(+route.paramMap.get('filmID')!).pipe(
    catchError(() => {
      router.navigate(['/admin/films']);
      notificationCenter.warning('Такого фильма в нашей библиотеке нету!');
      return EMPTY;
    }),
  );
};

export const quotesMapResolver: ResolveFn<QuotesMap | null> = (
  route,
  state,
) => {
  const dataAccessQuotesService = inject(DataAccessQuotesService);
  const router = inject(Router);
  const notificationCenter = inject(NOTIFY_TOKEN);
  return dataAccessQuotesService
    .getFilmQuotes(+route.paramMap.get('filmID')!)
    .pipe(
      tap((quotesMap: QuotesMap | null) => {
        if (!quotesMap) {
          router.navigate(['/admin/films']);
          notificationCenter.warning('Вы еще не добавили цитат к этому фильму');
        }
      }),
    );
};
