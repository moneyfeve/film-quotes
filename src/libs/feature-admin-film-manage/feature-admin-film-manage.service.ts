import { Injectable } from '@angular/core';
import {
  BehaviorSubject,
  map,
  Observable,
  of,
  Subject,
  switchMap,
  tap,
} from 'rxjs';
import { DataAccessFilmsService } from '../data-access-films/data-access-films.service';
import { Film, Films } from '../data-access-films/data-access-films.model';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';

@Injectable()
export class FeatureAdminFilmManageService {
  private filmID$: Subject<number | null> = new BehaviorSubject<number | null>(
    null,
  );
  private _cashedFilms: Film[] = [];

  constructor(
    private dataAccessFilmsService: DataAccessFilmsService,
    private dataAccessQuotesService: DataAccessQuotesService,
  ) {}

  getFilmId$() {
    return this.filmID$.asObservable();
  }

  setFilmId$(id: number | null) {
    return this.filmID$.next(id);
  }

  getFilmsByName$(query: string): Observable<Film[]> {
    return this.dataAccessFilmsService
      .getFilmsByName(query, { limit: 20 })
      .pipe(
        map((data: Films) => data.films),
        tap((films: Film[]) => {
          this._cashedFilms = films;
        }),
      );
  }

  getFilm$(): Observable<Film | null> {
    return this.getFilmId$().pipe(
      switchMap((filmID: number | null) => {
        if (!filmID) return of(null);
        const film: Film | undefined = this._cashedFilms.find(
          (film: Film) => film.id === filmID,
        );
        if (!!film) return of(film);
        return this.dataAccessFilmsService.getFilm(filmID);
      }),
    );
  }

  getFilmQuotesMap$() {
    return this.getFilmId$().pipe(
      switchMap((id: number | null) => {
        if (!id) return of(null);
        return this.dataAccessQuotesService.getFilmQuotes(id).pipe(
          map((quotesMap: QuotesMap | null): QuotesMap => {
            return {
              ...quotesMap,
              filmID: id,
              quotes: quotesMap?.quotes || [],
            };
          }),
        );
      }),
    );
  }
}
