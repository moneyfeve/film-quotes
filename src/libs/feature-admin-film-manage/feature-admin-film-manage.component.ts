import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  debounce,
  finalize,
  map,
  mergeWith,
  Observable,
  of,
  shareReplay,
  Subject,
  switchMap,
  take,
  tap,
  timer,
} from 'rxjs';
import { Film } from '../data-access-films/data-access-films.model';
import { NotificationCenterService } from '../../../projects/notification-center/src/lib/notification-center.service';
import { trim } from '../shared/util-formating/format-text';
import { DataAccessQuotesService } from '../data-access-quotes/data-access-quotes.service';
import { QuotesMap } from '../data-access-quotes/data-access-quotes.model';
import { ActivatedRoute, Data, Params } from '@angular/router';
import { QuotesService } from '../feature-admin-quotes-manage/quotes.service';
import { FeatureAdminFilmManageService } from './feature-admin-film-manage.service';

@Component({
  selector: 'feature-admin-film-manage',
  templateUrl: './feature-admin-film-manage.component.html',
  styleUrl: './feature-admin-film-manage.component.scss',
  providers: [FeatureAdminFilmManageService],
})
export class FeatureAdminFilmManageComponent implements OnInit {
  pageTitle: string = 'Управление цитатами';

  isEditMode$: Observable<boolean> | undefined;

  filmNameControl: FormControl<string | null> = new FormControl('');
  filmsDataList$: Observable<Film[]> | undefined;

  film$: Observable<Film | null> | undefined;
  filmName$: Observable<string> | undefined;
  filmQuotesMap$: Observable<QuotesMap | null> | undefined;
  filmQuotes$: Observable<string[]> | undefined;

  filmQuotesSubject: Subject<QuotesMap | null> =
    new Subject<QuotesMap | null>();

  constructor(
    private notificationCenter: NotificationCenterService,
    private featureService: FeatureAdminFilmManageService,
    private quotesService: QuotesService,
    private dataAccessQuotesService: DataAccessQuotesService,
    private route: ActivatedRoute,
  ) {}

  getRouteData$(name: string) {
    return this.route.data.pipe(map((data: Data) => data[name]));
  }

  ngOnInit() {
    this.isEditMode$ = this.route.params.pipe(
      map((params: Params) => !!params['filmID']),
    );

    this.filmQuotesMap$ = this.featureService
      .getFilmQuotesMap$()
      .pipe(
        mergeWith(
          this.getRouteData$('quotesMap'),
          this.filmQuotesSubject.asObservable(),
        ),
        shareReplay(1),
      );

    this.filmQuotes$ = this.filmQuotesMap$.pipe(
      map((quotesMap: QuotesMap | null) => {
        return (!!quotesMap && [...quotesMap.quotes]) || [];
      }),
    );

    this.film$ = this.featureService
      .getFilm$()
      .pipe(mergeWith(this.getRouteData$('filmData')), shareReplay(1));
    this.filmName$ = this.film$.pipe(
      map((film: Film | null) => (!!film && film.name) || ''),
    );

    this.filmsDataList$ = this.filmNameControl.valueChanges.pipe(
      map((s: string | null) => trim(s || '')),
      debounce((val) => {
        return !!val.length ? timer(440) : timer(0);
      }),
      switchMap((query: string): Observable<Film[]> => {
        if (!query.length) return of([]);
        return this.featureService.getFilmsByName$(query);
      }),
    );
  }

  resetFilm() {
    this.filmNameControl.reset();
    this.quotesService.resetQuotes();
    this.featureService.setFilmId$(null);
  }

  selectFilm(film: Film) {
    const { id } = film;
    if (!id) {
      return this.notificationCenter.info('Фильм не выбран');
    }
    this.featureService.setFilmId$(film.id);
  }

  saveQuotes() {
    this.filmQuotesMap$!.pipe(
      take(1),
      switchMap((quotesMap: QuotesMap | null) => {
        const data = {
          filmID: quotesMap?.filmID!,
          quotes: this.quotesService.getQuotes(),
        };
        console.log(quotesMap);
        if (quotesMap?.id) {
          return this.dataAccessQuotesService.updateQuotes(quotesMap.id, data);
        }
        return this.dataAccessQuotesService.createQuotes(data);
      }),
      tap((quotesMap: QuotesMap) => {
        setTimeout(() => {
          this.notificationCenter.success('Цитаты успешно сохранены');
          this.filmQuotesSubject.next(quotesMap);
        }, 10);
      }),
      take(1),
    ).subscribe();
  }
}
