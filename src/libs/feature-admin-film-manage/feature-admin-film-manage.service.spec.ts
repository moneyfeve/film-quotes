import { TestBed } from '@angular/core/testing';

import { FeatureAdminFilmManageService } from './feature-admin-film-manage.service';

describe('FeatureAdminFilmManageService', () => {
  let service: FeatureAdminFilmManageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FeatureAdminFilmManageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
