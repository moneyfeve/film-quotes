import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeatureAdminFilmManageComponent } from './feature-admin-film-manage.component';

describe('FeatureAdminFilmManageComponent', () => {
  let component: FeatureAdminFilmManageComponent;
  let fixture: ComponentFixture<FeatureAdminFilmManageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FeatureAdminFilmManageComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(FeatureAdminFilmManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
