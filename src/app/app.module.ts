import { NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { RouterOutlet } from '@angular/router';
import { AppRouterModule } from './app-router.module';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { environment } from '../environments/environment';
import { FB_TOKEN } from '../libs/environment/firebase-config.token';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NotificationCenterModule } from '../../projects/notification-center/src/lib/notification-center.module';
import { FeatureAdminHeaderModule } from '../libs/feature-admin-header/feature-admin-header.module';
import { KINOPOISK_TOKEN } from '../libs/environment/kinopoisk-config.token';
import { KinopoiskInterceptor } from '../libs/data-access-films/kinopoisk.interceptor';
import { AuthFirebaseInterceptor } from '../../projects/auth-firebase/src/lib/auth-firebase.interceptor';

const INTERCEPTOR_PROVIDERS: Provider[] = [
  { provide: HTTP_INTERCEPTORS, multi: true, useClass: KinopoiskInterceptor },
  {
    provide: HTTP_INTERCEPTORS,
    multi: true,
    useClass: AuthFirebaseInterceptor,
  },
];
@NgModule({
  declarations: [AppComponent, MainLayoutComponent, AdminLayoutComponent],
  imports: [
    BrowserModule,
    AppRouterModule,
    RouterOutlet,
    HttpClientModule,
    FeatureAdminHeaderModule,
    NotificationCenterModule.forRoot({
      duration: 3000,
      position: { x: 'right', y: 'bottom' },
    }),
  ],
  providers: [
    {
      provide: FB_TOKEN,
      useFactory: () => environment['firebaseConfig'],
    },
    {
      provide: KINOPOISK_TOKEN,
      useValue: environment['kinopoiskConfig'],
    },
    INTERCEPTOR_PROVIDERS,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
