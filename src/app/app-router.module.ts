import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminLayoutComponent } from './admin-layout/admin-layout.component';
import { MainLayoutComponent } from './main-layout/main-layout.component';
import { AuthFirebaseGuard } from '../../projects/auth-firebase/src/lib/auth-firebase.guard';
import { LoginGuard } from '../libs/feature-login-page/login.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    loadChildren: () =>
      import('../libs/feature-search-page/feature-search-page.module').then(
        (m) => m.FeatureSearchPageModule,
      ),
  },
  {
    path: 'films',
    component: MainLayoutComponent,
    loadChildren: () =>
      import('../libs/feature-film-page/feature-film-page.module').then(
        (m) => m.FeatureFilmPageModule,
      ),
  },
  {
    path: 'login',
    component: MainLayoutComponent,
    canActivate: [LoginGuard],
    loadChildren: () =>
      import('../libs/feature-login-page/feature-login-page.module').then(
        (m) => m.FeatureLoginPageModule,
      ),
  },
  {
    path: 'admin',
    component: AdminLayoutComponent,
    canActivate: [AuthFirebaseGuard],
    loadChildren: () =>
      import('../libs/feature-admin-shell/feature-admin-shell.module').then(
        (m) => m.FeatureAdminShellModule,
      ),
  },
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRouterModule {}
