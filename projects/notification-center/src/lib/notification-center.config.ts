import { InjectionToken } from '@angular/core';
import { NtfCenterConfig } from './notification.model';

export const NTF_CENTER_CONFIG = new InjectionToken<NtfCenterConfig>('config');
