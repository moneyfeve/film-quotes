export interface NtfCenterConfig {
  duration: number;
  position: {
    x: 'left' | 'right';
    y: 'top' | 'bottom';
  };
}
export enum NotificationTypes {
  success = 'success',
  info = 'info',
  warning = 'warning',
}

export interface NotificationOptionsSimple {
  title: string;
  duration: number;
}
export interface NotificationOptions {
  type: NotificationTypes;
  text: string;
  title?: string;
  duration?: number;
}

export interface Notification {
  id: string;
  timestamp?: number;
  type: NotificationTypes;
  title?: string;
  text: string;
  duration?: number;
  timeOutID?: number;
}

export interface Notifications {
  [key: string]: Notification;
}
