import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationItemComponent } from './components/notification-item/notification-item.component';
import { NotificationCenterComponent } from './components/notification-center/notification-center.component';
import { InfoIconComponent } from './components/icons/info-icon/info-icon.component';
import { SuccessIconComponent } from './components/icons/success-icon/info-icon.component';
import { WarningIconComponent } from './components/icons/warning-icon/info-icon.component';
import { NtfCenterConfig } from './notification.model';
import { NTF_CENTER_CONFIG } from './notification-center.config';
import { NotificationCenterService } from './notification-center.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    NotificationCenterComponent,
    NotificationItemComponent,
    InfoIconComponent,
    SuccessIconComponent,
    WarningIconComponent,
  ],
  providers: [NotificationCenterService],
  imports: [CommonModule, BrowserAnimationsModule],
  exports: [NotificationCenterComponent],
})
export class NotificationCenterModule {
  static forRoot(config: NtfCenterConfig): ModuleWithProviders<any> {
    return {
      ngModule: NotificationCenterModule,
      providers: [
        NotificationCenterService,
        {
          provide: NTF_CENTER_CONFIG,
          useValue: config,
        },
      ],
    };
  }
}
