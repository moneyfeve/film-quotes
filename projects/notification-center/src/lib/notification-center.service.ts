import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, tap } from 'rxjs';
import {
  Notification,
  NotificationOptions,
  NotificationOptionsSimple,
  Notifications,
  NotificationTypes,
  NtfCenterConfig,
} from './notification.model';
import { generateID } from './utils/notification-center.utils';
import { NTF_CENTER_CONFIG } from './notification-center.config';

@Injectable()
export class NotificationCenterService {
  private notifications: Notifications = {};
  private notifications$: BehaviorSubject<Notifications> =
    new BehaviorSubject<Notifications>({});

  constructor(@Inject(NTF_CENTER_CONFIG) private config: NtfCenterConfig) {
    this.initConfig();
  }

  initConfig() {
    if (!this.config) {
      this.config = {
        duration: 3000,
        position: { x: 'right', y: 'bottom' },
      };
    }
  }

  info(text: string, options?: Partial<NotificationOptionsSimple>) {
    this.create({ type: NotificationTypes.info, text, ...options });
  }

  success(text: string, options?: Partial<NotificationOptionsSimple>) {
    this.create({ type: NotificationTypes.success, text, ...options });
  }

  warning(text: string, options?: Partial<NotificationOptionsSimple>) {
    this.create({ type: NotificationTypes.warning, text, ...options });
  }

  create(data: NotificationOptions) {
    const notification: Notification = { id: generateID(), ...data };
    notification.timestamp = +new Date();
    !notification.duration && (notification.duration = this.config.duration);
    notification.timeOutID = this.delayDestroy(
      notification.id,
      notification.duration,
    );
    this.push(notification.id, notification);
  }

  private delayDestroy(id: string, delay: number) {
    return setTimeout(this.destroy.bind(this, id), delay);
  }

  destroy(id: string) {
    if (!this.notifications.hasOwnProperty(id)) return;
    this.notifications[id].timeOutID &&
      clearTimeout(this.notifications[id].timeOutID);
    delete this.notifications[id];
    this.notifications$.next(this.notifications);
  }

  getNotifications(): Observable<Notification[]> {
    return this.notifications$.pipe(
      map((notifications: Notifications) => {
        return Object.entries(notifications).map(([key, value]) => {
          return { ...value };
        });
      }),
      tap((notifications: Notification[]) => {
        this.config.position.y === 'top' && notifications.reverse();
      }),
    );
  }

  private push = (id: string, data: Notification) => {
    this.notifications[id] = data;
    this.notifications$.next(this.notifications);
  };
}
