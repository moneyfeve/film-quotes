import { Component, Input } from '@angular/core';
import { Notification, NotificationTypes } from '../../notification.model';
import { NotificationCenterService } from '../../notification-center.service';
import { animate, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'notification-item',
  templateUrl: './notification-item.component.html',
  styleUrl: './notification-item.component.scss',
  animations: [
    trigger('animationIn', [
      transition(':enter', [
        style({ opacity: 0, transform: 'scale(0.6)' }),
        animate('.18s', style({ opacity: 1, transform: 'scale(1)' })),
      ]),
    ]),
  ],
})
export class NotificationItemComponent {
  @Input() notification: Notification | undefined;
  protected readonly NotificationTypes = NotificationTypes;
  constructor(public service: NotificationCenterService) {}
}
