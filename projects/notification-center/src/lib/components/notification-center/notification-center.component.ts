import { Component, Inject } from '@angular/core';
import { NotificationCenterService } from '../../notification-center.service';
import { Notification, NtfCenterConfig } from '../../notification.model';
import { Observable } from 'rxjs';
import { NTF_CENTER_CONFIG } from '../../notification-center.config';

@Component({
  selector: 'notification-center',
  templateUrl: './notification-center.component.html',
  styleUrl: './notification-center.component.scss',
})
export class NotificationCenterComponent {
  notifications$: Observable<any> | null = null;

  constructor(
    public service: NotificationCenterService,
    @Inject(NTF_CENTER_CONFIG) public config: NtfCenterConfig,
  ) {
    this.notifications$ = service.getNotifications();
  }

  trackByFn(i: number, notice: Notification) {
    return notice.id;
  }
}
