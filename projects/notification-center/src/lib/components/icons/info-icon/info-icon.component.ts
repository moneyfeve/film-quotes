import { Component } from '@angular/core';

@Component({
  selector: 'info-icon',
  templateUrl: './info-icon.svg',
  styles: ['svg { max-width: 100%; height: auto }'],
})
export class InfoIconComponent {}
