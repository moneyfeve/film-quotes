import { Component } from '@angular/core';

@Component({
  selector: 'warning-icon',
  templateUrl: './warning-icon.svg',
  styles: ['svg { max-width: 100%; height: auto }'],
})
export class WarningIconComponent {}
