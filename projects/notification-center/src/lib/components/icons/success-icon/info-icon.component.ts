import { Component } from '@angular/core';

@Component({
  selector: 'success-icon',
  templateUrl: './success-icon.svg',
  styles: ['svg { max-width: 100%; height: auto }'],
})
export class SuccessIconComponent {}
