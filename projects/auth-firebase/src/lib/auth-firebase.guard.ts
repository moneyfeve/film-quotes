import { Injectable } from '@angular/core';
import { AuthFirebaseService } from './auth-firebase.service';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
} from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthFirebaseGuard implements CanActivate {
  constructor(
    private service: AuthFirebaseService,
    private router: Router,
  ) {}
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.service.isAuthenticated()) return true;

    this.router.navigate(['/login'], {
      queryParams: {
        returnUrl: state.url,
      },
    });
    return false;
  }
}
