export interface AuthUser {
    email: string;
    password: string;
    returnSecureToken?: boolean;
}
