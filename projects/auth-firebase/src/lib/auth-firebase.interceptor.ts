import { Inject, Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthFirebaseService } from './auth-firebase.service';
import { FB_TOKEN } from '../../../../src/libs/environment/firebase-config.token';
import { FirebaseConfig } from '../../../../src/libs/environment/firebase-config.model';

@Injectable()
export class AuthFirebaseInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthFirebaseService,
    @Inject(FB_TOKEN) private firebaseConfig: FirebaseConfig,
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    if (!req.url.includes(this.firebaseConfig.dataBaseUrl))
      return next.handle(req);
    if (this.authService.isAuthenticated()) {
      req = req.clone({
        setParams: {
          auth: this.authService.getToken(),
        },
      });
    }
    return next.handle(req);
  }
}
