import { NavigationExtras } from '@angular/router';

export interface NavigateParamsModel {
	url: any[];
	extras?: NavigationExtras;
}
