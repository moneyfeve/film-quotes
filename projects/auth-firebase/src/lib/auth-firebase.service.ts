import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FB_TOKEN } from '../../../../src/libs/environment/firebase-config.token';
import { FirebaseConfig } from '../../../../src/libs/environment/firebase-config.model';
import { AuthUser } from './auth-user.model';
import { Observable, tap } from 'rxjs';
import { StorageService } from './storage.service';
import { FirebaseAuthResponse } from './firebase-auth-responce.model';

@Injectable({
  providedIn: 'root',
})
export class AuthFirebaseService {
  storageTokenKey: string = 'fireBase-token';
  storageTokenExpKey: string = 'fireBase-token-exp';

  constructor(
    @Inject(FB_TOKEN) private firebaseConfig: FirebaseConfig,
    private http: HttpClient,
    private storageService: StorageService,
  ) {}

  getToken(): string {
    const expValue = this.storageService.getItem(this.storageTokenExpKey);
    const expDate = new Date(expValue);
    if (new Date() > expDate) {
      this.logout();
      return '';
    }
    return this.storageService.getItem(this.storageTokenKey);
  }

  fetch(user: AuthUser): Observable<FirebaseAuthResponse> {
    return this.http.post(
      `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${this.firebaseConfig.apiKey}`,
      user,
    );
  }

  login(user: AuthUser) {
    user.returnSecureToken = true;
    return this.fetch(user).pipe(tap(this.updateToken.bind(this)));
  }

  logout() {
    this.updateToken(null);
  }

  isAuthenticated(): boolean {
    return !!this.getToken();
  }

  static calcExpDate(expiresIn: string | number): Date {
    const currentTime = new Date().getTime();
    const expiresInMillisecond = +expiresIn * 1000;
    const expiresInTime = currentTime + expiresInMillisecond;

    return new Date(expiresInTime);
  }
  updateToken(res: FirebaseAuthResponse | null) {
    if (res) {
      this.storageService.setItem(this.storageTokenKey, res.idToken || '');
      this.storageService.setItem(
        this.storageTokenExpKey,
        AuthFirebaseService.calcExpDate(res.expiresIn || 3600).toString(),
      );
    } else {
      this.storageService.removeItem(this.storageTokenKey);
      this.storageService.removeItem(this.storageTokenExpKey);
    }
  }
}
